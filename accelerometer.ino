#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

const char *ssid = "test";
const char *password = "123456789";

File mySensorDataXYZ;
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);
unsigned long ido;
String filename4 = "XYZ.TXT";
String WebString = "";
int fsizeDisk = 0;

ESP8266WebServer server(80);

String home = "<h1>Accelerometer data</h1>"
              "<p><a href=\"down\"><button>Download</button></a>"
              "<p><a href=\"remove\"><button>Remove</button></a>";

void setup() {
  Serial.begin(115200);
  WiFi.softAP(ssid, password);
  IPAddress apip = WiFi.softAPIP();
  SD.begin(15);
  server.on("/", handleRoot);
  server.on("/down", handleDownload);
  server.on("/remove", fileRemove);
  server.begin();
  accel.begin();
  //Serial.println("Setup");
  //delay(1000);
}

void loop() {
  ido = millis() / 1000;
  sensors_event_t event;
  accel.getEvent(&event);
  char fileNameCharArray4[filename4.length() + 1];
  filename4.toCharArray(fileNameCharArray4, sizeof(fileNameCharArray4));

  //Serial.println("2. lepes File nev letrehozasa");
  //delay(1000);

  mySensorDataXYZ = SD.open(fileNameCharArray4, FILE_WRITE);
  mySensorDataXYZ.print(event.acceleration.x);
  mySensorDataXYZ.print(" ");
  mySensorDataXYZ.print(event.acceleration.y);
  mySensorDataXYZ.print(" ");
  mySensorDataXYZ.print(event.acceleration.z);
  mySensorDataXYZ.print(" ");
  mySensorDataXYZ.println(ido);

  //Serial.println("3. lepes adatok File-ba irasa");
  //delay(1000);

  mySensorDataXYZ.close();

  //Serial.println("4. lepes File bezarasa");
  //delay(1000);


  server.handleClient();
  //Serial.println("loop");
  //delay(1000);
}

void fileRemove()
{
  SD.remove("XYZ.TXT");
  server.send(200, "text/html", home);
}

void handleRoot() {
  server.send(200, "text/html", home);
  //Serial.println("Betoltott az oldal");
}

void handleDownload() {

  int32_t time = millis();
  mySensorDataXYZ = SD.open("XYZ.TXT");
  int fsizeDisk = mySensorDataXYZ.size();


  WebString = "";
  WebString += "HTTP/1.1 200 OK\r\n";
  WebString += "Content-Type: text/plain\r\n";
  WebString += "Content-Disposition: attachment; filename=\"XYZ.TXT\"\r\n";
  WebString += "Content-Length: " + String(fsizeDisk) + "\r\n";
  WebString += "\r\n";
  server.sendContent(WebString);

  char buf[1024];
  int siz = mySensorDataXYZ.size();
  while (siz > 0) {
    size_t len = std::min((int)(sizeof(buf) - 1), siz);
    mySensorDataXYZ.read((uint8_t *)buf, len);
    server.client().write((const char*)buf, len);
    siz -= len;
  }
  //Serial.println("Letoltesgomb megnyomasa");

  mySensorDataXYZ.close();
  time = millis() - time;
}



